= MME
:author: Andrew Tar
:revnumber: 1.0.0
:experimental:
:title: MME
:lang: ru
:showtitle:
:tabsize: 2
:media: print
:icons: image
:show-link-uri:
:pdf-page-layout: portrait
:nofooter:
:toc: auto
:imagesdir: ../images/
:title-page:

<<<

[plantuml, target=diagram-classes, format=png]
....
class BlockProcessor
class DiagramBlock
class DitaaBlock
class PlantUmlBlock

BlockProcessor <|-- DiagramBlock
DiagramBlock <|-- DitaaBlock
DiagramBlock <|-- PlantUmlBlock
....

<<<

include::alarm/index.adoc[]

<<<

include::basics/index.adoc[]

<<<

include::oam/index.adoc[]
