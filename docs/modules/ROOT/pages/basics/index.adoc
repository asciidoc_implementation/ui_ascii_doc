== Базовые сведения

include::functionalities.adoc[Функциональные возможности]

<<<

include::general.adoc[Общие сведения]

<<<

include::high_availability.adoc[Резервирование]

<<<

include::internal_architecture.adoc[Внутренняя архитектура]

<<<

include::lifecycle.adoc[Жизненный цикл]

<<<

include::network_architecture.adoc[Сетевая архитектура]

<<<

include::requirements_hw_sw.adoc[Системные требования]

<<<

include::roadmap.adoc[Оперативные планы]

<<<

include::scaling.adoc[Масштабирование]

<<<

include::specifications.adoc[Спецификации и рекомендации]
